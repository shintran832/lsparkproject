package main.Business;

import java.time.LocalDate;

public class Business {
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getNameOfLead() {
        return nameOfLead;
    }

    public void setNameOfLead(String nameOfLead) {
        this.nameOfLead = nameOfLead;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public LocalDate getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(LocalDate startAt) {
        this.startedAt = startAt;
    }

    public LocalDate getExpiredAt() {
        return expiredAt;
    }

    public void setExpiredAt(LocalDate expiredAt) {
        this.expiredAt = expiredAt;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    private String companyName,nameOfLead,description,phone,contract,address;
    private long companyId;
    private LocalDate startedAt,expiredAt;
    public Business(long companyId, String companyName,String phone, LocalDate startedAt, LocalDate expiredAt) {
        this.companyId = companyId;
        this.companyName = companyName;
        this.phone = phone;
        this.startedAt = startedAt;
        this.expiredAt = expiredAt;
    }

    public Business(String companyName, String nameOfLead, String description, String phone, LocalDate startedAt, LocalDate expiredAt, String contract, String address) {
        this.companyName = companyName;
        this.nameOfLead = nameOfLead;
        this.description = description;
        this.phone = phone;
        this.startedAt = startedAt;
        this.expiredAt = expiredAt;
        this.contract = contract;
        this.address = address;
    }

    public Business(long companyId, String companyName, String nameOfLead, String description, String phone, LocalDate startedAt, LocalDate expiredAt, String contract, String address) {
        this.companyId = companyId;
        this.companyName = companyName;
        this.nameOfLead = nameOfLead;
        this.description = description;
        this.phone = phone;
        this.startedAt = startedAt;
        this.expiredAt = expiredAt;
        this.contract = contract;
        this.address = address;
}
}

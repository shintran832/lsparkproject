package main.Business.Controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import main.Business.Business;
import main.Business.Model.BusinessModel;
import main.Controller;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;

public class BusinessController implements Initializable {
//    Khai báo biến cho tableView và 4 table Column
    @FXML
    private TableView<Business> table;
    @FXML
    private TableColumn<Business,String> idCompanyColumn;
    @FXML
    private TableColumn<Business,String> nameCompanyColumn;
    @FXML
    private TableColumn<Business,String> phoneColumn;
    @FXML
    private TableColumn<Business, LocalDate> startedAtColumn;
    @FXML
    private TableColumn<Business,LocalDate> expiredAtColumn;
    @FXML
    private TextField tfSearchBusiness;
    @FXML
    private Label lNameUser;
    private ObservableList<Business> list = FXCollections.observableArrayList(BusinessModel.fetchAllBusiness());
// Constructor thực hiện việc thêm danh sách doanh nghiep vào tableView , sort theo idColumn, tra cứu
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initData();
        table.getSortOrder().addAll(idCompanyColumn);
        advanceSearch();
        setNameUser();
    }

//    Hàm thêm danh sách nhân viên vào tableView
    @FXML
    private void initData() {
        this.idCompanyColumn.setCellValueFactory(new PropertyValueFactory<Business, String>("companyId"));
        this.nameCompanyColumn.setCellValueFactory(new PropertyValueFactory<Business, String>("companyName"));
        this.phoneColumn.setCellValueFactory(new PropertyValueFactory<Business, String>("phone"));
        this.startedAtColumn.setCellValueFactory(new PropertyValueFactory<Business, LocalDate>("startedAt"));
        this.expiredAtColumn.setCellValueFactory(new PropertyValueFactory<Business, LocalDate>("expiredAt"));
        table.setItems(list);
    }
    // Chuyển sang trang Thêm doanh nghiep
    @FXML
    private void changeViewModalCreate(ActionEvent event) {
        try {
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../View/ModalCreate.fxml"));
            Parent modalCreate = loader.load();
            Scene scene = new Scene(modalCreate);
            stage.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
// Xoá doanh nghiep
    @FXML
    private void delBusiness(ActionEvent event) {
        Business selected = table.getSelectionModel().getSelectedItem();
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setHeaderText(null);
        alert.setContentText("Do you really want to delete these record? This process cannot be undone");
        ButtonType buttonTypeOK = new ButtonType("OK",ButtonBar.ButtonData.OK_DONE);
        ButtonType buttonTypeCancel = new ButtonType("Cancel",ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(buttonTypeCancel,buttonTypeOK);
        Optional<ButtonType> response = alert.showAndWait();
        if(response.get()== buttonTypeOK) {
            boolean res = BusinessModel.delBusiness(selected);
            if (res) {
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText(null);
                alert.setContentText("Delete Business Success!!");
                alert.showAndWait();
                list.remove(selected);
            } else {
                alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setContentText("Error! Delete Business fail!");
                alert.showAndWait();
            }
        }
    }
//    Chuyen sang trang Cập nhật doanh nghiep
    @FXML
    private void changeViewModalDetail(ActionEvent event) {
        try {
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../View/ModalDetail.fxml"));
            Parent modalCreate = loader.load();
            Scene scene = new Scene(modalCreate);
            ModalDetailController controller = loader.getController();
            long selectedId = table.getSelectionModel().getSelectedItem().getCompanyId();
            Business selected = BusinessModel.getInfo(selectedId);
            controller.setBusiness(selected);
            stage.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
//    Tra cứu
    @FXML
    private void advanceSearch() {
        FilteredList<Business> filteredList = new FilteredList<>(list, b -> true);
        tfSearchBusiness.textProperty().addListener((observable , oldValue, newValue) -> {
            filteredList.setPredicate(business -> {
                if(newValue ==null || newValue.isEmpty()) {
                    return true;
                }
                String lowCaseFilter = newValue.toLowerCase();
                if(String.valueOf(business.getCompanyId()).toLowerCase().indexOf(lowCaseFilter) != -1 ) {
                    return true;
                }
                else if(business.getCompanyName().toLowerCase().indexOf(lowCaseFilter)!= -1) {
                    return true;
                }
                else if (business.getPhone().toLowerCase().indexOf(lowCaseFilter) != -1) {
                    return true;
                }
                else if (String.valueOf(business.getStartedAt()).toLowerCase().indexOf(lowCaseFilter) != -1) {
                    return true;
                }
                else if (String.valueOf(business.getExpiredAt()).toLowerCase().indexOf(lowCaseFilter) != -1) {
                    return true;
                }
                else
                    return false;
            });
        });
        SortedList<Business> sortData = new SortedList<>(filteredList);
        sortData.comparatorProperty().bind(table.comparatorProperty());
        table.setItems(sortData);
        table.getSortOrder().add(idCompanyColumn);
    }
    public void changeViewDashboard(ActionEvent event) {
        Controller.changeViewDashboard(event);
    }
    public void changeViewEmployee(ActionEvent event) {
        Controller.changeViewEmployee(event);
    }
    public void changeViewLogin(ActionEvent event) {
        Controller.changeViewLogin(event);
    }
    public void changeViewOrder(ActionEvent event) {
        Controller.changeViewOrder(event);
    }
    public void changeViewPOS(ActionEvent event) {
        Controller.changeViewPOS(event);
    }
    public void changeViewTicket(ActionEvent event) {
        Controller.changeViewTicket(event);
    }
    public void changeViewWarehouse(ActionEvent event) {
        Controller.changeViewWarehouse(event);
    }
    public void changeViewCustomer(ActionEvent event) {
        Controller.changeViewCustomer(event);
    }
    @FXML
    private void setNameUser() {
        lNameUser.setText(Controller.employee.getName());
    }
}

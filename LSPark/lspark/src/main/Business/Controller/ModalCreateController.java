package main.Business.Controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import main.Business.Business;
import main.Business.Model.ModalCreateModel;
import main.Controller;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class ModalCreateController implements Initializable {
    @FXML
    private TextField tfNameCompany;
    @FXML
    private TextField tfNameOfLead;
    @FXML
    private TextField tfPhone;
    @FXML
    private TextField tfAddress;
    @FXML
    private DatePicker tfStartedAt;
    @FXML
    private DatePicker tfExpiredAt;
    @FXML
    private TextArea tfContract;
    @FXML
    private TextArea tfDescription;

// Constructor thực hiện setItem( gồm male và female cho genderList )
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @FXML
    private void createNewBusiness(ActionEvent event) {
        String nameCompanyBsn = tfNameCompany.getText();
        String nameOfLeadBsn = tfNameOfLead.getText();
        String phoneBsn = tfPhone.getText();
        String addressBsn = tfAddress.getText();
        LocalDate startedAtBsn = tfStartedAt.getValue();
        LocalDate expriedAtBsn = tfExpiredAt.getValue();
        String contractBsn = tfContract.getText();
        String descriptionBsn = tfDescription.getText();
        Alert alert;
        if(nameCompanyBsn.isEmpty() || nameOfLeadBsn.isEmpty() || phoneBsn.isEmpty() || addressBsn.isEmpty() || String.valueOf(startedAtBsn).isEmpty() || String.valueOf(expriedAtBsn).isEmpty() || contractBsn.isEmpty() || descriptionBsn.isEmpty()) {
            alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setContentText("You are missing some data!");
            alert.showAndWait();
            return;
        }
        Business bsnNew = new Business(nameCompanyBsn,nameOfLeadBsn,descriptionBsn,phoneBsn,startedAtBsn,expriedAtBsn,contractBsn,addressBsn);
        boolean res = ModalCreateModel.createNewBusiness(bsnNew);
        if(res == true) {
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Create New Business Success!!!");
            alert.showAndWait();
            changeViewBusiness(event);
        }
        else {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("Error Invalid information !!!");
            alert.showAndWait();
        }
    }
    @FXML
    private void changeViewBusiness(ActionEvent event) {
        Controller.changeViewBusiness(event);
    }
}

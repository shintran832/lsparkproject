package main.Business.Controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import main.Business.Business;
import main.Business.Model.ModalDetailModel;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
public class ModalDetailController implements Initializable {
    @FXML
    private TextField tfNameCompany;
    @FXML
    private TextField tfNameOfLead;
    @FXML
    private TextField tfPhone;
    @FXML
    private TextField tfAddress;
    @FXML
    private DatePicker tfStartedAt;
    @FXML
    private DatePicker tfExpiredAt;
    @FXML
    private TextArea tfContract;
    @FXML
    private TextArea tfDescription;

    private long ID ;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
    public void setBusiness(Business selected) {
        tfNameCompany.setText(selected.getCompanyName());
        tfNameOfLead.setText(selected.getNameOfLead());
        tfStartedAt.setValue(selected.getStartedAt());
        tfExpiredAt.setValue(selected.getExpiredAt());
        tfAddress.setText(selected.getAddress());
        tfContract.setText(selected.getContract());
        tfPhone.setText(selected.getPhone());
        tfDescription.setText(selected.getDescription());
        ID = selected.getCompanyId();
    }
    public void updateBusiness(ActionEvent event) {
        String nameCompanyBsn = tfNameCompany.getText();
        String nameOfLeadBsn = tfNameOfLead.getText();
        String phoneBsn = tfPhone.getText();
        String addressBsn = tfAddress.getText();
        LocalDate startedAtBsn = tfStartedAt.getValue();
        LocalDate expriedAtBsn = tfExpiredAt.getValue();
        String contractBsn = tfContract.getText();
        String descriptionBsn = tfDescription.getText();
        Alert alert;
        if(nameCompanyBsn.isEmpty() || nameOfLeadBsn.isEmpty() || phoneBsn.isEmpty() || addressBsn.isEmpty() || String.valueOf(startedAtBsn).isEmpty() || String.valueOf(expriedAtBsn).isEmpty() || contractBsn.isEmpty() || descriptionBsn.isEmpty()) {
            alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setContentText("You are missing some data!");
            alert.showAndWait();
            return;
        }
        Business bsn = new Business(nameCompanyBsn,nameOfLeadBsn,descriptionBsn,phoneBsn,startedAtBsn,expriedAtBsn,contractBsn,addressBsn);
        boolean res = ModalDetailModel.updateBusiness(bsn,ID);
        if(res == true) {
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Update New Business Success!!!");
            alert.showAndWait();
            changeViewBusiness(event);
        }
        else {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("Error Invalid information !!!");
            alert.showAndWait();
        }
    }
    @FXML
    private void changeViewBusiness(ActionEvent event) {
        try {
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../View/Business.fxml"));
            Parent Employees = loader.load();
            Scene scene = new Scene(Employees);
            stage.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

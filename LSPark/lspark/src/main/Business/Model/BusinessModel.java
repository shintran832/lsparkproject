package main.Business.Model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.Business.Business;
import main.JDBCConection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class BusinessModel {
//    LOAD DATA
    public static ObservableList<Business> fetchAllBusiness() {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlFetchAll = "SELECT * FROM BUSINESS";
        ObservableList<Business> listBusiness = FXCollections.observableArrayList();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlFetchAll);
            ResultSet res = preparedStatement.executeQuery();
            while (res.next()) {
                long companyId = res.getLong("COMPANY_ID");
                String companyName = res.getString("COMPANY_NAME");
                String phone = res.getString("PHONE_BUSINESS");
                LocalDate startedAt = res.getDate("STARTED_AT").toLocalDate();
                LocalDate expired_at = res.getDate("EXPIRED_AT").toLocalDate();
                listBusiness.add(new Business(companyId,companyName,phone,startedAt,expired_at));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listBusiness;
    }
    public static boolean delBusiness(Business bsn) {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlDelete = "DELETE FROM BUSINESS WHERE COMPANY_ID=?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlDelete);
            preparedStatement.setLong(1,bsn.getCompanyId());
            int res = preparedStatement.executeUpdate();
            if(res>0) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public static Business getInfo(long ID) {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlGetInfo = "SELECT * FROM BUSINESS WHERE COMPANY_ID = ?";
        Business business = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlGetInfo);
            preparedStatement.setLong(1,ID);
            ResultSet res = preparedStatement.executeQuery();
            while(res.next()) {
                long companyId = res.getLong("COMPANY_ID");
                String companyName = res.getString("COMPANY_NAME");
                String nameOfLead = res.getString("NAME_OF_LEAD");
                String phone = res.getString("PHONE_BUSINESS");
                LocalDate startedAt = res.getDate("STARTED_AT").toLocalDate();
                LocalDate expired_at = res.getDate("EXPIRED_AT").toLocalDate();
                String contract = res.getString("CONTRACT");
                String address = res.getString("ADDRESS");
                String description = res.getString("DESCRIPTION");
                business = new Business(companyId,companyName,nameOfLead,description,phone,startedAt,expired_at,contract,address);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return business;
    }

}

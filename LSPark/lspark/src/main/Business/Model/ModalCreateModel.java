package main.Business.Model;

import main.Business.Business;
import main.Employee.Employee;
import main.JDBCConection;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ModalCreateModel {
    public static boolean createNewBusiness(Business bsnNew) {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlCreate = "INSERT INTO BUSINESS (COMPANY_ID,COMPANY_NAME,NAME_OF_LEAD,PHONE_BUSINESS,STARTED_AT,EXPIRED_AT,CONTRACT,ADDRESS,DESCRIPTION) VALUES (S_BUSINESS.NEXTVAL,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlCreate);
            preparedStatement.setString(1,bsnNew.getCompanyName());
            preparedStatement.setString(2,bsnNew.getNameOfLead());
            preparedStatement.setString(3,bsnNew.getPhone());
            preparedStatement.setDate(4, Date.valueOf(bsnNew.getStartedAt()));
            preparedStatement.setDate(5, Date.valueOf(bsnNew.getExpiredAt()));
            preparedStatement.setString(6,bsnNew.getContract());
            preparedStatement.setString(7,bsnNew.getAddress());
            preparedStatement.setString(8,bsnNew.getDescription());
            int res = preparedStatement.executeUpdate();
            if(res >0) return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}

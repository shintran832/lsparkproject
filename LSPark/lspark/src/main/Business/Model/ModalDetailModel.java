package main.Business.Model;

import javafx.fxml.FXML;
import main.Business.Business;
import main.JDBCConection;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ModalDetailModel {

    @FXML
    public static boolean updateBusiness(Business bsn, long ID) {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlUpdate = "UPDATE BUSINESS SET COMPANY_NAME= ?, NAME_OF_LEAD= ?, PHONE_BUSINESS= ?,STARTED_AT= ?, EXPIRED_AT= ?, CONTRACT= ?, ADDRESS= ?, DESCRIPTION= ? WHERE COMPANY_ID=?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlUpdate);
            preparedStatement.setString(1,bsn.getCompanyName());
            preparedStatement.setString(2,bsn.getNameOfLead());
            preparedStatement.setString(3,bsn.getPhone());
            preparedStatement.setDate(4, Date.valueOf(bsn.getStartedAt()));
            preparedStatement.setDate(5, Date.valueOf(bsn.getExpiredAt()));
            preparedStatement.setString(6,bsn.getContract());
            preparedStatement.setString(7,bsn.getAddress());
            preparedStatement.setString(8,bsn.getDescription());
            preparedStatement.setLong(9,ID);
            int res = preparedStatement.executeUpdate();
            if(res >0) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}

package main;


import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.Employee.Employee;

import javax.swing.*;
import java.io.IOException;

public class Controller {
    public static Employee employee;
    public static void changeView (ActionEvent event, String path) {
        try {
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Controller.class.getResource(path));
            Parent parent = loader.load();
            Scene scene = new Scene(parent);
            stage.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void changeViewDashboard(ActionEvent event) {
        if(Controller.employee.getPosition().equals("Director"))
            Controller.changeView(event,"Dashboard/View/Dashboard.fxml");
        else if(Controller.employee.getPosition().equals("Manager"))
            Controller.changeView(event,"Dashboard/View/DashboardMan.fxml");
        else
            Controller.changeView(event,"Dashboard/View/DashboardEmp.fxml");
    }
    public static void changeViewEmployee(ActionEvent event) {
        if(Controller.employee.getPosition().equals("Director"))
            changeView(event,"Employee/View/Employee.fxml");
        else
            changeView(event,"Employee/View/EmployeeMan.fxml");
    }
    public static void changeViewCustomer(ActionEvent event) {
        if(Controller.employee.getPosition().equals("Director"))
            changeView(event,"Customer/View/Customer.fxml");
        else if(Controller.employee.getPosition().equals("Manager"))
            changeView(event,"Customer/View/CustomerMan.fxml");
        else
            changeView(event,"Customer/View/CustomerEmp.fxml");
    }
    public static void changeViewTicket(ActionEvent event) {
        if(Controller.employee.getPosition().equals("Director"))
            changeView(event,"Ticket/View/Ticket.fxml");
        else if(Controller.employee.getPosition().equals("Manager"))
            changeView(event,"Ticket/View/TicketMan.fxml");
    }
    public static void changeViewWarehouse(ActionEvent event) {
        if(Controller.employee.getPosition().equals("Director"))
            changeView(event,"Warehouse/View/Warehouse.fxml");
        else if(Controller.employee.getPosition().equals("Manager"))
            changeView(event,"Warehouse/View/Warehouse.fxml");
        else
            changeView(event,"Warehouse/View/Warehouse.fxml");
    }
    public static void changeViewOrder(ActionEvent event) {
        if(Controller.employee.getPosition().equals("Director"))
            changeView(event,"Order/View/listOrder.fxml");
        else if(Controller.employee.getPosition().equals("Manager"))
            changeView(event,"Order/View/listOrderMan.fxml");
    }
    public static void changeViewPOS(ActionEvent event) {
        if(Controller.employee.getPosition().equals("Director"))
            changeView(event,"Order/View/PointOfSales.fxml");
        else if(Controller.employee.getPosition().equals("Manager"))
            changeView(event,"Order/View/PointOfSalesMan.fxml");
        else
            changeView(event,"Order/View/PointOfSalesEmp.fxml");
    }
    public static void changeViewBusiness(ActionEvent event) {
        changeView(event,"Business/View/Business.fxml");
    }
    public static void changeViewLogin(ActionEvent event) {
        changeView(event,"Login/View/Login.fxml");
    }

    public static void setEmployee(Employee emp) {
        employee=emp;
    }


}

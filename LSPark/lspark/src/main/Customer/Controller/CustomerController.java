package main.Customer.Controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import main.Controller;
import main.Customer.Customer;
import main.Customer.Model.CustomerModel;
import main.Employee.Controller.ModalDetailController;
import main.Employee.Model.EmployeeModel;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class CustomerController implements Initializable {
    @FXML
    private TableView<Customer> table;
    @FXML
    private TableColumn<Customer,String> idColumn;
    @FXML
    private TableColumn<Customer,String> nameColumn;
    @FXML
    private TableColumn<Customer,String> genderColumn;
    @FXML
    private TableColumn<Customer,String> phoneColumn;
    @FXML
    private TableColumn<Customer,String> typeColumn;
    @FXML
    private TextField tfSearchCustomer;
    private ObservableList<Customer> list = FXCollections.observableArrayList(CustomerModel.fetchAllCustomers());
    @FXML
    private Label lTotalVIP;
    @FXML
    private Label lTotalLoyal;
    @FXML
    private Label lTotalNormal;
    @FXML
    private Label lNameUser;
    //    Khai bao 3 bien thong ke position
    int totalVIP=0;
    int totalLoyal=0;
    int totalNormal=0;

    // Constructor thực hiện việc thêm danh sách nhân viên vào tableView , sort theo idColumn, tra cứu
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initData();
        initInfoCalculation();
        table.getSortOrder().addAll(idColumn);
        advanceSearch();
        setNameUser();
    }
    //    Thong ke VIP, Loyal, Normal
    @FXML
    public void initInfoCalculation() {
        for (int i=0;i<list.size();i++) {
            if(list.get(i).getType().equals("VIP"))
                totalVIP += 1;
            else if(list.get(i).getType().equals("Loyal"))
                totalLoyal += 1;
            else totalNormal += 1;
        }
        lTotalVIP.setText(String.valueOf(totalVIP));
        lTotalLoyal.setText(String.valueOf(totalLoyal));
        lTotalNormal.setText(String.valueOf(totalNormal));
    }
    //    Hàm thêm danh sách khách hàng vào tableView
    @FXML
    private void initData() {
        this.idColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("id"));
        this.nameColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("name"));
        this.genderColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("gender"));
        this.phoneColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("phone"));
        this.typeColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("type"));
        table.setItems(list);
    }
    // Chuyển sang trang Thêm khách hàng
    @FXML
    private void changeViewModalCustomerCreate(ActionEvent event) {
        try {
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../View/CreateCustomer.fxml"));
            Parent modalCreate = loader.load();
            Scene scene = new Scene(modalCreate);
            stage.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // Xoá khách hàng
    @FXML
    private void delCustomer(ActionEvent event) {
        Customer selected = table.getSelectionModel().getSelectedItem();
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setHeaderText(null);
        alert.setContentText("Do you really want to delete this record? This process cannot be undone");
        ButtonType buttonTypeOK = new ButtonType("OK",ButtonBar.ButtonData.OK_DONE);
        ButtonType buttonTypeCancel = new ButtonType("Cancel",ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(buttonTypeCancel,buttonTypeOK);
        Optional<ButtonType> response = alert.showAndWait();
        if(response.get()== buttonTypeOK) {
            boolean res = CustomerModel.delCustomer(selected);
            if (res) {
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText(null);
                alert.setContentText("Delete Customer Success!!");
                alert.showAndWait();
                list.remove(selected);
                if(selected.getType().equals("VIP")) {
                    totalVIP-=1;
                    lTotalVIP.setText(String.valueOf(totalVIP));
                }
                else if(selected.getType().equals("Loyal")){
                    totalLoyal-=1;
                    lTotalLoyal.setText(String.valueOf(totalLoyal));
                }
                else {
                    totalNormal-=1;
                    lTotalNormal.setText(String.valueOf(totalNormal));
                }
            } else {
                alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setContentText("Error! Delete Customer fail!");
                alert.showAndWait();
            }
        }
    }
    //    Chuyen sang trang Cập nhật khách hàng
    @FXML
    private void changeViewModalCustomerDetail(ActionEvent event) {
        try {
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../View/DetailCustomer.fxml"));
            Parent modalCreate = loader.load();
            Scene scene = new Scene(modalCreate);
            CustomerDetailController controller = loader.getController();
            String selectedId = table.getSelectionModel().getSelectedItem().getId();
            Customer selected = CustomerModel.getInfo(selectedId);
            controller.setCustomer(selected);
            stage.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //    Tra cứu
    @FXML
    private void advanceSearch() {
        FilteredList<Customer> filteredList = new FilteredList<>(list, b -> true);
        tfSearchCustomer.textProperty().addListener((observable , oldValue, newValue) -> {
            filteredList.setPredicate(customer -> {
                if(newValue ==null || newValue.isEmpty()) {
                    return true;
                }
                String lowCaseFilter = newValue.toLowerCase();
                if(customer.getId().toLowerCase().indexOf(lowCaseFilter) != -1 ) {
                    return true;
                }
                else if(customer.getName().toLowerCase().indexOf(lowCaseFilter)!= -1) {
                    return true;
                }
                else if (customer.getGender().toLowerCase().indexOf(lowCaseFilter) != -1) {
                    return true;
                }
                else if (customer.getType().toLowerCase().indexOf(lowCaseFilter) != -1) {
                    return true;
                }
                else if (customer.getPhone().toLowerCase().indexOf(lowCaseFilter) != -1) {
                    return true;
                }
                else
                    return false;
            });
        });
        SortedList<Customer> sortData = new SortedList<>(filteredList);
        sortData.comparatorProperty().bind(table.comparatorProperty());
        table.setItems(sortData);
        table.getSortOrder().add(idColumn);
    }
    public void changeViewEmployee(ActionEvent event) {
        Controller.changeViewEmployee(event);
    }
    public void changeViewDashboard(ActionEvent event) {
        Controller.changeViewDashboard(event);
    }
    public void changeViewBusiness(ActionEvent event) {
        Controller.changeViewBusiness(event);
    }
    public void changeViewLogin(ActionEvent event) {
        Controller.changeViewLogin(event);
    }
    public void changeViewOrder(ActionEvent event) {
        Controller.changeViewOrder(event);
    }
    public void changeViewPOS(ActionEvent event) {
        Controller.changeViewPOS(event);
    }
    public void changeViewTicket(ActionEvent event) {
        Controller.changeViewTicket(event);
    }
    public void changeViewWarehouse(ActionEvent event) {
        Controller.changeViewWarehouse(event);
    }
    @FXML
    private void setNameUser() {
        lNameUser.setText(Controller.employee.getName());
    }
}


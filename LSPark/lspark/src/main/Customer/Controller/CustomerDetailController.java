package main.Customer.Controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.Controller;
import main.Customer.Customer;
import main.Customer.Model.CustomerDetailModel;
import main.Employee.Employee;
import main.Employee.Model.ModalDetailModel;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class CustomerDetailController implements Initializable {
    @FXML
    private TextField id;
    @FXML
    private TextField name;
    @FXML
    private ComboBox<String> comboBoxGen ;
    ObservableList<String> genderlist = FXCollections.observableArrayList("Male","Female");
    @FXML
    private DatePicker birthDay;
    @FXML
    private TextField email;
    @FXML
    private TextField phone;
    @FXML
    private ComboBox<String> comboBoxType;
    ObservableList<String> typeList = FXCollections.observableArrayList("VIP","Loyal","Normal");
    @FXML
    private TextField Address;
    @FXML
    private TextField Score;
    private String ID = "";

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        comboBoxGen.setItems(genderlist);
        comboBoxType.setItems(typeList);
    }
    public void setCustomer(Customer selected) {
        id.setText(selected.getId());
        name.setText(selected.getName());
        comboBoxGen.setValue(selected.getGender());
        birthDay.setValue(selected.getBirthday());
        email.setText(selected.getEmail());
        comboBoxType.setValue(selected.getType());
        Address.setText(selected.getAddress());
        Score.setText(String.valueOf(selected.getScore()));
        phone.setText(selected.getPhone());
        ID = selected.getId();
    }
    public void updateCustomer(ActionEvent event) {
        String idCus = id.getText();
        String nameCus = name.getText();
        String gender = (String) comboBoxGen.getValue();
        LocalDate birthDayCus = birthDay.getValue();
        String emailCus = email.getText();
        String phoneCus = phone.getText();
        String typeCus = comboBoxType.getValue();
        String addressCus = Address.getText();
        long scoreCus = Long.parseLong(Score.getText());
        Alert alert;
        if(idCus.isEmpty() || nameCus.isEmpty() || gender.isEmpty() || String.valueOf(birthDayCus).isEmpty()  || emailCus.isEmpty() || phoneCus.isEmpty() || addressCus.isEmpty() || typeCus.isEmpty() || String.valueOf(scoreCus).isEmpty()) {
            alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setContentText("You are missing some data!");
            alert.showAndWait();
            return;
        }
        Customer cusNew = new Customer(idCus,nameCus,gender,birthDayCus,phoneCus,scoreCus,typeCus,addressCus,emailCus);
        boolean res = CustomerDetailModel.updateCustomer(cusNew,ID);
        if(res) {
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Update New Customer Success");
            alert.showAndWait();
            changeViewCustomer(event);
        }
        else {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("Error!! Invalid information");
            alert.showAndWait();
        }
    }
    @FXML
    private void changeViewCustomer(ActionEvent event) {
        Controller.changeViewCustomer(event);
    }

}

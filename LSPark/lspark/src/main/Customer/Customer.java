package main.Customer;

import java.time.LocalDate;

public class Customer {
    private String id, name, gender, phone, type, address, email;
    private LocalDate birthday;
    private long score;

    public Customer(String id, String name, String gender, LocalDate birthday, String phone, long score, String type, String address, String email) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.birthday = birthday;
        this.phone = phone;
        this.score = score;
        this.type = type;
        this.address = address;
        this.email = email;
    }

    public Customer(String id, String name, String gender, String phone, String type) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.phone = phone;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public String getPhone() {
        return phone;
    }

    public long getScore() {
        return score;
    }

    public String getType() {
        return type;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

package main.Customer.Model;

import main.Customer.Customer;
import main.Employee.Employee;
import main.JDBCConection;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CustomerCreateModel {
    public static boolean createNewCustomer(Customer cusNew) {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlCreate = "INSERT INTO CUSTOMER (CUS_ID, CUS_NAME, GENDER, BIRTHDAY, PHONE, SCORE, TYPE, ADDRESS, EMAIL) VALUES (?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlCreate);
            preparedStatement.setString(1,cusNew.getId());
            preparedStatement.setString(2,cusNew.getName());
            preparedStatement.setString(3,cusNew.getGender());
            preparedStatement.setDate(4, Date.valueOf(cusNew.getBirthday()));
            preparedStatement.setString(5,cusNew.getPhone());
            preparedStatement.setString(6, String.valueOf(0));
            preparedStatement.setString(7,cusNew.getType());
            preparedStatement.setString(8,cusNew.getAddress());
            preparedStatement.setString(9,cusNew.getEmail());
            int res = preparedStatement.executeUpdate();
            if(res ==1) return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}

package main.Customer.Model;

import javafx.fxml.FXML;
import main.Customer.Customer;
import main.Employee.Employee;
import main.JDBCConection;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CustomerDetailModel {

    @FXML
    public static boolean updateCustomer(Customer cus, String cusID) {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlUpdate = "UPDATE CUSTOMER SET CUS_ID= ?, CUS_NAME= ?, GENDER= ?, BIRTHDAY= ?,PHONE= ?, SCORE= ?, TYPE= ?, ADDRESS= ?, EMAIL= ? WHERE CUS_ID=?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlUpdate);
            preparedStatement.setString(1,cus.getId());
            preparedStatement.setString(2,cus.getName());
            preparedStatement.setString(3,cus.getGender());
            preparedStatement.setDate(4, Date.valueOf(cus.getBirthday()));
            preparedStatement.setString(5,cus.getPhone());
            preparedStatement.setLong(6,cus.getScore());
            preparedStatement.setString(7,cus.getType());
            preparedStatement.setString(8,cus.getAddress());
            preparedStatement.setString(9,cus.getEmail());
            preparedStatement.setString(10,cusID);
            int res = preparedStatement.executeUpdate();
            if(res ==1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}

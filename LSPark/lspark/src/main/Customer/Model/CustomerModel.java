package main.Customer.Model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.Customer.Customer;
import main.Employee.Employee;
import main.JDBCConection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class CustomerModel {
    //    LOAD DATA
    public static ObservableList<Customer> fetchAllCustomers() {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlFetchAll = "SELECT * FROM CUSTOMER";
        ObservableList<Customer> listCustomer = FXCollections.observableArrayList();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlFetchAll);
            ResultSet res = preparedStatement.executeQuery();
            while (res.next()) {
                String id = res.getString("CUS_ID");
                String name = res.getString("CUS_NAME");
                String gender = res.getString("GENDER");
                String phone = res.getString("PHONE");
                String type = res.getString("TYPE");
                listCustomer.add(new Customer(id,name,gender,phone,type));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listCustomer;
    }
    public static boolean delCustomer(Customer cus) {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlDelete = "DELETE FROM CUSTOMER WHERE CUS_ID=?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlDelete);
            preparedStatement.setString(1,cus.getId());
            int res = preparedStatement.executeUpdate();
            if(res==1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public static Customer getInfo(String ID) {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlGetInfo = "SELECT * FROM CUSTOMER WHERE CUS_ID = ?";
        Customer customer = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlGetInfo);
            preparedStatement.setString(1,ID);
            ResultSet res = preparedStatement.executeQuery();
            while(res.next()) {
                String id = res.getString("CUS_ID");
                String name = res.getString("CUS_NAME");
                String gender = res.getString("GENDER");
                LocalDate birthDay = res.getDate("BIRTHDAY").toLocalDate();
                String email = res.getString("EMAIL");
                long score = res.getLong("SCORE");
                String address = res.getString("ADDRESS");
                String phone = res.getString("PHONE");
                String type = res.getString("TYPE");
                customer = new Customer(id,name,gender,birthDay,phone,score,type,address,email);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

}

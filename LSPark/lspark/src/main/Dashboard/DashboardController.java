package main.Dashboard;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import main.Controller;
import main.Employee.Controller.ModalDetailController;
import main.Employee.Employee;
import main.Employee.Model.EmployeeModel;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class DashboardController implements Initializable {
    @FXML
    private Label lTotalEmp;
    @FXML
    private Label lNameUser;
    ObservableList <Employee> listEmp ;
    int countEmp=0;
    int countBsn=0;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        getInfoReport();
        setNameUser();
    }
    public void getInfoReport() {
        listEmp = EmployeeModel.fetchAllEmployees();
        countEmp = listEmp.size();
        lTotalEmp.setText(String.valueOf(countEmp));
    }
    @FXML
    public void changeViewEmployee(ActionEvent event) {
        Controller.changeViewEmployee(event);
    }
    @FXML
    public void changeViewCustomer(ActionEvent event) {
        Controller.changeViewCustomer(event);
    }
    @FXML
    public void changeViewBusiness(ActionEvent event) {
        Controller.changeViewBusiness(event);
    }
    @FXML
    private void changeViewLogin(ActionEvent event) {
        Controller.changeViewLogin(event);
    }
    @FXML
    public void changeViewTicket(ActionEvent event) { Controller.changeViewTicket(event);}
    @FXML
    public void changeViewWarehouse(ActionEvent event) {
        Controller.changeViewWarehouse(event);
    }
    @FXML
    public void changeViewPOS(ActionEvent event) {
        Controller.changeViewPOS(event);
    }
    @FXML
    public void changeViewOrder(ActionEvent event) {
        Controller.changeViewOrder(event);
    }
    @FXML
    private void setNameUser() {
        lNameUser.setText(Controller.employee.getName());
    }
}

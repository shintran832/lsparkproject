package main.Employee.Controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import main.Controller;
import main.Employee.Employee;
import main.Employee.Model.EmployeeModel;
import main.Employee.Model.ModalCreateModel;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class EmployeeController implements Initializable {
//    Khai báo biến cho tableView và 4 table Column
    @FXML
    private TableView<Employee> table;
    @FXML
    private TableColumn<Employee,String> idColumn;
    @FXML
    private TableColumn<Employee,String> nameColumn;
    @FXML
    private TableColumn<Employee,String> genderColumn;
    @FXML
    private TableColumn<Employee,String> positionColumn;
    @FXML
    private TableColumn<Employee,String> phoneColumn;
    @FXML
    private TextField tfSearchEmployee;
    public ObservableList<Employee> list = FXCollections.observableArrayList(EmployeeModel.fetchAllEmployees());
    @FXML
    Label lTotalEmployee;
    @FXML
    Label lTotalManager;
    @FXML
    Label lTotalDirector;
    @FXML
    private Label lNameUser;
//    Khai bao 3 bien thong ke position
    int totalEmployee=0;
    int totalManager=0;
    int totalDirector=0;

// Constructor thực hiện việc thêm danh sách nhân viên vào tableView , sort theo idColumn, tra cứu
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initData();
        initInfoCalculation();
        table.getSortOrder().addAll(idColumn);
        advanceSearch();
        setNameUser();
    }
//    Viet cac ham changeView o day
    @FXML
    public void changeViewDashboard(ActionEvent event) {
        Controller.changeViewDashboard(event);
    }
    @FXML
    public void changeViewBusiness(ActionEvent event) {
        Controller.changeViewBusiness(event);
    }
    @FXML
    public void changeViewCustomer(ActionEvent event) {
        Controller.changeViewCustomer(event);
    }
    @FXML
    public void changeViewTicket(ActionEvent event) {
        Controller.changeViewTicket(event);
    }
    @FXML
    public void changeViewOrder(ActionEvent event) {
        Controller.changeViewOrder(event);
    }
    @FXML
    public void changeViewPOS(ActionEvent event) {
        Controller.changeViewPOS(event);
    }
    @FXML
    public void changeViewWarehouse(ActionEvent event) {
        Controller.changeViewWarehouse(event);
    }

//    Thong ke director,manager,employee
    @FXML
    public void initInfoCalculation() {
        for (int i=0;i<list.size();i++) {
            if(list.get(i).getPosition().equals("Director"))
                totalDirector += 1;
            else if(list.get(i).getPosition().equals("Manager"))
                totalManager += 1;
            else totalEmployee += 1;
        }
        lTotalDirector.setText(String.valueOf(totalDirector));
        lTotalManager.setText(String.valueOf(totalManager));
        lTotalEmployee.setText(String.valueOf(totalEmployee));
    }
//    Hàm thêm danh sách nhân viên vào tableView
    @FXML
    public void initData() {
        this.idColumn.setCellValueFactory(new PropertyValueFactory<Employee, String>("id"));
        this.nameColumn.setCellValueFactory(new PropertyValueFactory<Employee, String>("name"));
        this.genderColumn.setCellValueFactory(new PropertyValueFactory<Employee, String>("gender"));
        this.positionColumn.setCellValueFactory(new PropertyValueFactory<Employee, String>("position"));
        this.phoneColumn.setCellValueFactory(new PropertyValueFactory<Employee, String>("phone"));
        table.setItems(list);
    }
    // Chuyển sang trang Thêm nhân viên
    @FXML
    public void changeViewModalCreate(ActionEvent event) {
        try {
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../View/ModalCreate.fxml"));
            Parent modalCreate = loader.load();
            Scene scene = new Scene(modalCreate);
            stage.setScene(scene);
//            Stage stage = new Stage();
//            FXMLLoader loader = new FXMLLoader(getClass().getResource("../View/ModalCreate.fxml"));
//            Parent modalCreate = loader.load();
//            Scene scene = new Scene(modalCreate);
//            stage.initModality(Modality.APPLICATION_MODAL);
//            stage.setScene(scene);
//            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
// Xoá nhân viên
    @FXML
    private void delEmployee(ActionEvent event) {
        Employee selected = table.getSelectionModel().getSelectedItem();
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setHeaderText(null);
        alert.setContentText("Do you really want to delete these record? This process cannot be undone");
        ButtonType buttonTypeOK = new ButtonType("OK",ButtonBar.ButtonData.OK_DONE);
        ButtonType buttonTypeCancel = new ButtonType("Cancel",ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(buttonTypeCancel,buttonTypeOK);
        Optional<ButtonType> response = alert.showAndWait();
        if(response.get()== buttonTypeOK) {
            boolean res = EmployeeModel.delEmployee(selected);
            if (res) {
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText(null);
                alert.setContentText("Delete Employee Success!!");
                alert.showAndWait();
                list.remove(selected);
                if(selected.getPosition().equals("Director")) {
                    totalDirector-=1;
                    lTotalDirector.setText(String.valueOf(totalDirector));
                }
                else if(selected.getPosition().equals("Manager")){
                    totalManager-=1;
                    lTotalManager.setText(String.valueOf(totalManager));
                }
                else {
                    totalEmployee-=1;
                    lTotalEmployee.setText(String.valueOf(totalEmployee));
                }
            } else {
                alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setContentText("Error! Delete Employee fail!");
                alert.showAndWait();
            }
        }
    }
//    Chuyen sang trang Cập nhật nhân viên
    @FXML
    private void changeViewModalDetail(ActionEvent event) {
        try {
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../View/ModalDetail.fxml"));
            Parent modalCreate = loader.load();
            Scene scene = new Scene(modalCreate);
            ModalDetailController controller = loader.getController();
            String selectedId = table.getSelectionModel().getSelectedItem().getId();
            Employee selected = EmployeeModel.getInfo(selectedId);
            controller.setEmployee(selected);
            stage.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
//    Tra cứu
    @FXML
    private void advanceSearch() {
        FilteredList<Employee> filteredList = new FilteredList<>(list, b -> true);
        tfSearchEmployee.textProperty().addListener((observable , oldValue, newValue) -> {
            filteredList.setPredicate(employee -> {
                if(newValue ==null || newValue.isEmpty()) {
                    return true;
                }
                String lowCaseFilter = newValue.toLowerCase();
                if(employee.getId().toLowerCase().indexOf(lowCaseFilter) != -1 ) {
                    return true;
                }
                else if(employee.getName().toLowerCase().indexOf(lowCaseFilter)!= -1) {
                    return true;
                }
                else if (employee.getGender().toLowerCase().indexOf(lowCaseFilter) != -1) {
                    return true;
                }
                else if (employee.getPosition().toLowerCase().indexOf(lowCaseFilter) != -1) {
                    return true;
                }
                else if (employee.getPhone().toLowerCase().indexOf(lowCaseFilter) != -1) {
                    return true;
                }
                else
                    return false;
            });
        });
        SortedList<Employee> sortData = new SortedList<>(filteredList);
        sortData.comparatorProperty().bind(table.comparatorProperty());
        table.setItems(sortData);
        table.getSortOrder().add(idColumn);
    }
    @FXML
    private void setNameUser() {
        lNameUser.setText(Controller.employee.getName());
    }
    @FXML
    private void changeViewLogin(ActionEvent event) {
        Controller.changeViewLogin(event);
    }
//    @FXML
//    public void refreshData(Employee empNew) {
//        list.add(empNew);
//        if(empNew.getPosition().equals("Director")) {
//            totalDirector+=1;
//            lTotalDirector.setText(String.valueOf(totalDirector));
//        }
//        else if(empNew.getPosition().equals("Manager")) {
//            totalManager+=1;
//            lTotalManager.setText(String.valueOf(totalManager));
//        }
//        else {
//            totalEmployee+=1;
//            lTotalEmployee.setText(String.valueOf(totalEmployee));
//        }
//    }

}

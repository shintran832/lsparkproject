package main.Employee.Controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import main.Controller;
import main.Employee.Employee;
import main.Employee.Model.ModalCreateModel;
import main.Employee.Model.ModalDetailModel;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.ResourceBundle;
public class ModalDetailController implements Initializable {
    @FXML
    private TextField id;
    @FXML
    private TextField name;
    @FXML
    private ComboBox<String> comboBox ;
    ObservableList<String> genderlist = FXCollections.observableArrayList("Male","Female");
    @FXML
    private DatePicker birthDay;
    @FXML
    private DatePicker startDate;
    @FXML
    private TextField email;
    @FXML
    private TextField phone;
    @FXML
    private ComboBox<String> positionBox;
    ObservableList<String> positionList = FXCollections.observableArrayList("Director","Manager","Employee");
    @FXML
    private TextField idManger;
    @FXML
    private TextField Address;
    @FXML
    private PasswordField password;

    private String ID = "";

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        comboBox.setItems(genderlist);
        positionBox.setItems(positionList);
    }
    public void setEmployee(Employee selected) {
        id.setText(selected.getId());
        name.setText(selected.getName());
        comboBox.setValue(selected.getGender());
        birthDay.setValue(selected.getBirthday());
        startDate.setValue(selected.getStartDate());
        email.setText(selected.getEmail());
        positionBox.setValue(selected.getPosition());
        Address.setText(selected.getAddress());
        phone.setText(selected.getPhone());
        idManger.setText(selected.getManager_id());
        password.setText(selected.getPassword());
        ID = selected.getId();
    }
    public void updateEmployee(ActionEvent event) {
        String idEmp = id.getText();
        String nameEmp = name.getText();
        String gender = (String) comboBox.getValue();
        LocalDate birthDayEmp = birthDay.getValue();
        LocalDate startDateEmp = startDate.getValue();
        String emailEmp = email.getText();
        String phoneEmp = phone.getText();
        String positionEmp = positionBox.getValue();
        String idManagerEmp = idManger.getText();
        String addressEmp = Address.getText();
        String passwordEmp = password.getText();
        Alert alert;
        if(idEmp.isEmpty() || nameEmp.isEmpty() || gender.isEmpty() || birthDayEmp==null || startDateEmp==null || emailEmp.isEmpty() || phoneEmp.isEmpty() || addressEmp.isEmpty() || positionEmp.isEmpty() || idManagerEmp.isEmpty() || passwordEmp.isEmpty()) {
            alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setContentText("You are missing some data!");
            alert.showAndWait();
            return;
        }
        Employee empNew = new Employee(idEmp,nameEmp,gender,birthDayEmp,emailEmp,positionEmp,addressEmp,phoneEmp,startDateEmp,idManagerEmp,passwordEmp);
        boolean res = ModalDetailModel.updateEmployee(empNew,ID);
        if(res == true) {
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Update New Employee Success");
            alert.showAndWait();
            changeViewEmployee(event);
        }
        else {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("Error!! Invalid information");
            alert.showAndWait();
        }
    }
    @FXML
    private void changeViewEmployee(ActionEvent event) {
        Controller.changeViewEmployee(event);
    }
}

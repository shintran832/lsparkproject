package main.Employee;

import java.sql.Date;
import java.time.LocalDate;

public class Employee {
    private String id,name,gender,email,position,address,phone,manager_id,password;
    private LocalDate birthday,startDate;
    public Employee(String id, String name, String gender, String position, String phone, String password) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.position = position;
        this.phone = phone;
        this.password = password;
    }

    public Employee(String id, String name, String gender, LocalDate birthday, String email, String position, String address, String phone, LocalDate startDate, String manager_id, String password) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.birthday = birthday;
        this.email = email;
        this.position = position;
        this.address = address;
        this.phone = phone;
        this.startDate = startDate;
        this.manager_id = manager_id;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public String getEmail() {
        return email;
    }

    public String getPosition() {
        return position;
    }

    public String getManager_id() {
        return manager_id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public String getGender() {
        return gender;
    }
}

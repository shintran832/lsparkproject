package main.Employee.Model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import jdk.nashorn.internal.scripts.JD;
import main.Employee.Employee;
import main.JDBCConection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Date;

public class EmployeeModel {
//    LOAD DATA
    public static ObservableList<Employee> fetchAllEmployees() {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlFetchAll = "SELECT * FROM EMPLOYEE";
        ObservableList<Employee> listEmployees = FXCollections.observableArrayList();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlFetchAll);
            ResultSet res = preparedStatement.executeQuery();
            while (res.next()) {
                String id = res.getString("EMP_ID");
                String name = res.getString("EMP_NAME");
                String gender = res.getString("GENDER");
                String position = res.getString("POSITION");
                String phone = res.getString("PHONE");
                String password = res.getString("PASSWORD");
                listEmployees.add(new Employee(id,name,gender,position,phone,password));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listEmployees;
    }
    public static boolean delEmployee(Employee emp) {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlDelete = "DELETE FROM EMPLOYEE WHERE EMP_ID=?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlDelete);
            preparedStatement.setString(1,emp.getId());
            int res = preparedStatement.executeUpdate();
            if(res==1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public static Employee getInfo(String ID) {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlGetInfo = "SELECT * FROM EMPLOYEE WHERE EMP_ID = ?";
        Employee employee = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlGetInfo);
            preparedStatement.setString(1, ID);
            ResultSet res = preparedStatement.executeQuery();
            while (res.next()) {
                String id = res.getString("EMP_ID");
                String name = res.getString("EMP_NAME");
                String gender = res.getString("GENDER");
                LocalDate birthDay = res.getDate("BIRTHDAY").toLocalDate();
                String email = res.getString("EMAIL");
                String position = res.getString("POSITION");
                String address = res.getString("ADDRESS");
                String phone = res.getString("PHONE");
                LocalDate startDate = res.getDate("STARTDATE").toLocalDate();
                String manager_id = res.getString("MANAGER_ID");
                String password = res.getString("PASSWORD");
                employee = new Employee(id, name, gender, birthDay, email, position, address, phone, startDate, manager_id,password);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return employee;
    }

}

package main.Employee.Model;

import main.Employee.Employee;
import main.JDBCConection;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ModalCreateModel {
    public static boolean createNewEmployee(Employee empNew) {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlCreate = "INSERT INTO EMPLOYEE (EMP_ID,EMP_NAME,GENDER,BIRTHDAY,EMAIL,POSITION,ADDRESS,PHONE,STARTDATE,MANAGER_ID,PASSWORD) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlCreate);
            preparedStatement.setString(1,empNew.getId());
            preparedStatement.setString(2,empNew.getName());
            preparedStatement.setString(3,empNew.getGender());
            preparedStatement.setDate(4, Date.valueOf(empNew.getBirthday()));
            preparedStatement.setString(5,empNew.getEmail());
            preparedStatement.setString(6,empNew.getPosition());
            preparedStatement.setString(7,empNew.getAddress());
            preparedStatement.setString(8,empNew.getPhone());
            preparedStatement.setDate(9, Date.valueOf(empNew.getStartDate()));
            preparedStatement.setString(10,empNew.getManager_id());
            preparedStatement.setString(11,empNew.getPassword());
            int res = preparedStatement.executeUpdate();
            if(res ==1) return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}

package main.Employee.Model;

import javafx.fxml.FXML;
import main.Employee.Employee;
import main.JDBCConection;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ModalDetailModel {

    @FXML
    public static boolean updateEmployee(Employee emp, String empID) {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlUpdate = "UPDATE EMPLOYEE SET EMP_ID= ?, EMP_NAME= ?, GENDER= ?, BIRTHDAY= ?,EMAIL= ?, POSITION= ?, ADDRESS= ?, PHONE= ?, STARTDATE= ?, MANAGER_ID= ?, PASSWORD= ? WHERE EMP_ID=?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlUpdate);
            preparedStatement.setString(1,emp.getId());
            preparedStatement.setString(2,emp.getName());
            preparedStatement.setString(3,emp.getGender());
            preparedStatement.setDate(4, Date.valueOf(emp.getBirthday()));
            preparedStatement.setString(5,emp.getEmail());
            preparedStatement.setString(6,emp.getPosition());
            preparedStatement.setString(7,emp.getAddress());
            preparedStatement.setString(8,emp.getPhone());
            preparedStatement.setDate(9, Date.valueOf(emp.getStartDate()));
            preparedStatement.setString(10,emp.getManager_id());
            preparedStatement.setString(11,emp.getPassword());
            preparedStatement.setString(12,empID);
            int res = preparedStatement.executeUpdate();
            if(res ==1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}

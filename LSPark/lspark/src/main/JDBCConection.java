package main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConection {
    public static Connection getJDBCConection() {
        String url = "jdbc:oracle:thin:@//localhost:1521/orcl";
        String userName = "DEMO";
        String passWord = "DEMO";
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            try {
                return DriverManager.getConnection(url,userName,passWord);
            } catch(SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}

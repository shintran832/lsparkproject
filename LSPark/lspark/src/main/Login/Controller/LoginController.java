package main.Login.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.sun.deploy.association.Action;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import main.Controller;
import main.Employee.Employee;
import main.Employee.Model.EmployeeModel;

import java.io.IOException;


public class LoginController {
    @FXML
    private JFXButton btnLogin ;
    @FXML
    private JFXButton btnCancel;
    @FXML
    private JFXTextField tfUsername;
    @FXML
    private JFXPasswordField tfPassword;

    private ObservableList<Employee> listEmp = FXCollections.observableArrayList(EmployeeModel.fetchAllEmployees());

    @FXML
    public void checkLogin(ActionEvent event) {
        for(int i=0;i<listEmp.size();i++)
            if(listEmp.get(i).getId().equals(tfUsername.getText()) && listEmp.get(i).getPassword().equals(tfPassword.getText())) {
                Controller.setEmployee(listEmp.get(i));
                if(Controller.employee.getPosition().equals("Director"))
                    Controller.changeView(event,"Dashboard/View/Dashboard.fxml");
                else if(Controller.employee.getPosition().equals("Manager"))
                    Controller.changeView(event,"Dashboard/View/DashboardMan.fxml");
                else
                    Controller.changeView(event,"Dashboard/View/DashboardEmp.fxml");
                return;
            }
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText(null);
        alert.setContentText("Error, Wrong username or password!!!");
        alert.showAndWait();
    }

}

package main.Order.Controller;

import com.sun.tools.corba.se.idl.constExpr.Or;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import main.Controller;
import main.Employee.Employee;
import main.Order.Model.OrderModel;
import main.Order.Order;

import javax.swing.*;
import java.net.URL;
import java.sql.Connection;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

public class OrderController implements Initializable {
    @FXML
    private TableView<Order> table;
    @FXML
    private TableColumn<Order,String> idColumn;
    @FXML
    private TableColumn<Order,String> idCusColumn;
    @FXML
    private TableColumn<Order,String> idTicColumn;
    @FXML
    private TableColumn<Order,String> idEmpColumn;
    @FXML
    private TableColumn<Order,LocalDateTime> timeOfTranColumn;
    @FXML
    private TableColumn<Order,Long> totalPriceColumn;
    @FXML
    private TextField tfSearch;
    @FXML
    private Label lTotalOrders;
    @FXML
    private Label lTotalMoney;
    @FXML
    private Label lNameUser;
    int totalOrders =0;
    long totalMoney =0;
    public ObservableList<Order> list = FXCollections.observableArrayList(OrderModel.fetchAllOrders());
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initData();
        advanceSearch();
        initInfoCalculation();
        setNameUser();
    }
    public void initData() {
        this.idColumn.setCellValueFactory(new PropertyValueFactory<Order, String>("order_Id"));
        this.idCusColumn.setCellValueFactory(new PropertyValueFactory<Order, String>("cus_Id"));
        this.idTicColumn.setCellValueFactory(new PropertyValueFactory<Order, String>("tic_Id"));
        this.idEmpColumn.setCellValueFactory(new PropertyValueFactory<Order, String>("emp_Id"));
        this.timeOfTranColumn.setCellValueFactory(new PropertyValueFactory<Order, LocalDateTime>("timeOfTrans"));
        this.totalPriceColumn.setCellValueFactory(new PropertyValueFactory<Order, Long>("totalPrice"));
        table.setItems(list);
    }
    @FXML
    private void advanceSearch() {
        FilteredList<Order> filteredList = new FilteredList<>(list, b -> true);
        tfSearch.textProperty().addListener((observable , oldValue, newValue) -> {
            filteredList.setPredicate(order -> {
                if(newValue ==null || newValue.isEmpty()) {
                    return true;
                }
                String lowCaseFilter = newValue.toLowerCase();
                if(order.getOrder_Id().toLowerCase().indexOf(lowCaseFilter) != -1 ) {
                    return true;
                }
                else if(order.getCus_Id().indexOf(lowCaseFilter) != -1) {
                    return true;
                }
                else if (order.getTic_Id().indexOf(lowCaseFilter) != -1) {
                    return true;
                }
                else if (order.getEmp_Id().indexOf(lowCaseFilter) != -1) {
                    return true;
                }
                else if (String.valueOf(order.getTotalPrice()).indexOf(lowCaseFilter) != -1) {
                    return true;
                }
                else if (String.valueOf(order.getTimeOfTrans()).indexOf(lowCaseFilter) != -1) {
                    return true;
                }
                else
                    return false;
            });
        });
        SortedList<Order> sortData = new SortedList<>(filteredList);
        sortData.comparatorProperty().bind(table.comparatorProperty());
        table.setItems(sortData);
        table.getSortOrder().add(idColumn);
    }
    public void initInfoCalculation() {
        totalOrders = Integer.parseInt(list.get(list.size()-1).getOrder_Id());
        for(int i=0;i<list.size();i++) {
            totalMoney += list.get(i).getTotalPrice();
        }
        lTotalOrders.setText(String.valueOf(totalOrders));
        lTotalMoney.setText(String.valueOf(totalMoney));
    }
    @FXML
    public void changeViewDashboard(ActionEvent event) {
        Controller.changeViewDashboard(event);
    }
    @FXML
    public void changeViewEmployee(ActionEvent event) {
        Controller.changeViewEmployee(event);
    }
    @FXML
    public void changeViewCustomer(ActionEvent event) {
        Controller.changeViewCustomer(event);
    }
    @FXML
    public void changeViewBusiness(ActionEvent event) {
        Controller.changeViewBusiness(event);
    }
    @FXML
    public void changeViewTicket(ActionEvent event) {
        Controller.changeViewTicket(event);
    }
    @FXML
    public void changeViewLogin(ActionEvent event) {
        Controller.changeViewLogin(event);
    }
    @FXML
    public void changeViewPOS(ActionEvent event) {
        Controller.changeViewPOS(event);
    }
    @FXML
    public void changeViewWarehouse(ActionEvent event) {
        Controller.changeViewWarehouse(event);
    }
    @FXML
    private void setNameUser() {
        lNameUser.setText(Controller.employee.getName());
    }
}

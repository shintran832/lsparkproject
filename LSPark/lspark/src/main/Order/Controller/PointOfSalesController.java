package main.Order.Controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import main.Controller;
import main.Customer.Customer;
import main.Customer.Model.CustomerModel;
import main.Employee.Employee;
import main.Order.Model.PointOfSalesModel;
import main.Order.Order;
import main.Order.Pos;

import javax.swing.*;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.ResourceBundle;

public class PointOfSalesController implements Initializable {
    @FXML
    public TableView<Pos> tableOrder;
    @FXML
    public TableColumn<Pos,String> idOrderColumn;
    @FXML
    public TableColumn<Pos,String> nameOrderColumn;
    @FXML
    public TableColumn<Pos,String> quantityOrderColumn;
    @FXML
    public TableColumn<Pos,String> priceOrderColumn;
    @FXML
    public TableView<Pos> tableItem;
    @FXML
    public TableColumn<Pos,String> nameItemColumn;
    @FXML
    public TableColumn<Pos,String> quantityItemColumn;
    @FXML
    public TableColumn<Pos,String> priceItemColumn;
    @FXML
    private TextField tfSearchItem;
    @FXML
    private Label lName;
    @FXML
    private Label lPrice;
    @FXML
    private JFXTextField jCusId;
    @FXML
    public JFXTextField jQuantity;
    @FXML
    private Label lSubTotal;
    @FXML
    private Label lTotal;
    @FXML
    private Label lDiscount;
    @FXML
    private Label lCusName;
    @FXML
    private Label lNameUser;

    public ObservableList <Pos> list = FXCollections.observableArrayList(PointOfSalesModel.fetchAllItem());
    public ObservableList<Pos> itemList = FXCollections.observableArrayList();
    public ObservableList<Customer> listCustomer = FXCollections.observableArrayList(CustomerModel.fetchAllCustomers());
    public Pos selected;
    LocalDateTime timeOfTran = LocalDateTime.now();
    public Order ordering = new Order(0,"","","",0,timeOfTran);
    public ObservableList <Order> listOrders = FXCollections.observableArrayList();
    long subTotal=0;
    long total =0;
    long discount = 0;
    String cusId;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initData();
        initItem();
        advanceSearch();
        selectItemInTableItem();
        selectItemInTableOrder();
        setNameUser();
    }
    @FXML
    public void changeViewEmployee(ActionEvent event) {
        Controller.changeViewEmployee(event);
    }
    @FXML
    public void changeViewCustomer(ActionEvent event) {
        Controller.changeViewCustomer(event);
    }
    @FXML
    public void changeViewBusiness(ActionEvent event) {
        Controller.changeViewBusiness(event);
    }
    @FXML
    public void changeViewLogin(ActionEvent event) {
        Controller.changeViewLogin(event);
    }
    @FXML
    public void changeViewTicket(ActionEvent event) { Controller.changeViewTicket(event);}
    @FXML
    public void changeViewWarehouse(ActionEvent event) {
        Controller.changeViewWarehouse(event);
    }
    @FXML
    public void changeViewDashboard(ActionEvent event) {
        Controller.changeViewDashboard(event);
    }
    @FXML
    public void changeViewOrder(ActionEvent event) {
        Controller.changeViewOrder(event);
    }
    @FXML
    public void initData() {
        this.idOrderColumn.setCellValueFactory(new PropertyValueFactory<Pos, String>("tic_Id"));
        this.nameOrderColumn.setCellValueFactory(new PropertyValueFactory<Pos, String>("name"));
        this.quantityOrderColumn.setCellValueFactory(new PropertyValueFactory<Pos, String>("quantity"));
        this.priceOrderColumn.setCellValueFactory(new PropertyValueFactory<Pos, String>("price"));
        tableOrder.setItems(list);
    }
    @FXML
    public void initItem() {
        this.nameItemColumn.setCellValueFactory(new PropertyValueFactory<Pos, String>("name"));
        this.quantityItemColumn.setCellValueFactory(new PropertyValueFactory<Pos, String>("quantity"));
        this.priceItemColumn.setCellValueFactory(new PropertyValueFactory<Pos, String>("price"));
        tableItem.setItems(itemList);
    }
    @FXML
    private void advanceSearch() {
        FilteredList<Pos> filteredList = new FilteredList<>(list, b -> true);
        tfSearchItem.textProperty().addListener((observable , oldValue, newValue) -> {
            filteredList.setPredicate(item -> {
                if(newValue ==null || newValue.isEmpty()) {
                    return true;
                }
                String lowCaseFilter = newValue.toLowerCase();
                if(item.getTic_Id().toLowerCase().indexOf(lowCaseFilter) != -1 ) {
                    return true;
                }
                else if(item.getName().toLowerCase().indexOf(lowCaseFilter)!= -1) {
                    return true;
                }
                else if (item.getPrice().toLowerCase().indexOf(lowCaseFilter) != -1) {
                    return true;
                }
                else if (item.getQuantity().toLowerCase().indexOf(lowCaseFilter) != -1) {
                    return true;
                }
                else
                    return false;
            });
        });
        SortedList<Pos> sortData = new SortedList<>(filteredList);
        sortData.comparatorProperty().bind(tableOrder.comparatorProperty());
        tableOrder.setItems(sortData);
        tableOrder.getSortOrder().add(idOrderColumn);
    }

    public void selectItem(ActionEvent event) {
        String id = tableOrder.getSelectionModel().getSelectedItem().getTic_Id();
        String name = tableOrder.getSelectionModel().getSelectedItem().getName();
        String price = tableOrder.getSelectionModel().getSelectedItem().getPrice();
        selected = new Pos(id,name,"0",price);
        lName.setText(name);
        lPrice.setText(price);
    }
    public void selectItemInTableItem() {
        tableItem.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                String id = tableOrder.getSelectionModel().getSelectedItem().getTic_Id();
                String name = tableItem.getSelectionModel().getSelectedItem().getName();
                String price = tableItem.getSelectionModel().getSelectedItem().getPrice();
                String quantity = tableItem.getSelectionModel().getSelectedItem().getQuantity();
                selected = new Pos(id,name,"0",price);
                lName.setText(name);
                lPrice.setText(price);
                jQuantity.setText(quantity);
            }
        });
    }
    public void selectItemInTableOrder() {
        tableOrder.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                String id = tableOrder.getSelectionModel().getSelectedItem().getTic_Id();
                String name = tableOrder.getSelectionModel().getSelectedItem().getName();
                String price = tableOrder.getSelectionModel().getSelectedItem().getPrice();
                selected = new Pos(id,name,"0",price);
                jQuantity.setText("0");
                lName.setText(name);
                lPrice.setText(price);
            }
        });
    }

    public void addToCart(ActionEvent event) {
        selected.setQuantity(jQuantity.getText());
        if(selected.getQuantity().isEmpty() || Integer.parseInt(selected.getQuantity())<=0 || jCusId.getText().isEmpty() ) {
           Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("Error Invalid information !!!");
            alert.showAndWait();
        }
        else if(tableOrder.getSelectionModel().getSelectedItem().equals("Unlimited")) {
            itemList.add(selected);
            for(int i=0;i<listCustomer.size();i++) {
                if(jCusId.getText().equals(listCustomer.get(i).getId())) {
                    lCusName.setText(listCustomer.get(i).getName());
                    cusId = listCustomer.get(i).getId();
                }
            }
            subTotal+=Long.parseLong(selected.getPrice())*Long.parseLong(jQuantity.getText());
            for(int i=0;i<listCustomer.size();i++) {
                if(jCusId.getText().equals(listCustomer.get(i).getId()) && listCustomer.get(i).getType().equals("VIP")) {
                    discount = 10;
                    break;
                }
                else if(jCusId.getText().equals(listCustomer.get(i).getId()) && listCustomer.get(i).getType().equals("Loyal")) {
                    discount = 5;
                    break;
                }
                else {
                    discount =0;
                    break;
                }
            }
            lDiscount.setText(String.valueOf(discount));
            total = subTotal - (discount*subTotal)/100 ;
            lSubTotal.setText(String.valueOf(subTotal));
            lTotal.setText(String.valueOf(total));
            long totalPriceAfterDiscount = Long.parseLong(selected.getPrice())*Long.parseLong(jQuantity.getText())- (Long.parseLong(selected.getPrice())*Long.parseLong(jQuantity.getText())*discount)/100;
            ordering = new Order(totalPriceAfterDiscount,Controller.employee.getId(),cusId,selected.getTic_Id(),Integer.parseInt(jQuantity.getText()),LocalDateTime.now());
            listOrders.add(ordering);
        }
        else if(Integer.parseInt(tableOrder.getSelectionModel().getSelectedItem().getQuantity()) < Integer.parseInt(jQuantity.getText())) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("You don't have enough ticket to sales. Please update warehouse to sales !!!");
            alert.showAndWait();
        }
        else {
            itemList.add(selected);
            for(int i=0;i<listCustomer.size();i++) {
                if(jCusId.getText().equals(listCustomer.get(i).getId())) {
                    lCusName.setText(listCustomer.get(i).getName());
                    cusId = listCustomer.get(i).getId();
                }
            }
            subTotal+=Long.parseLong(selected.getPrice())*Long.parseLong(jQuantity.getText());
            for(int i=0;i<listCustomer.size();i++) {
                if(jCusId.getText().equals(listCustomer.get(i).getId()) && listCustomer.get(i).getType().equals("VIP")) {
                    discount = 10;
                    break;
                }
                else if(jCusId.getText().equals(listCustomer.get(i).getId()) && listCustomer.get(i).getType().equals("Loyal")) {
                    discount = 5;
                    break;
                }
                else {
                    discount =0;
                    break;
                }
            }
            lDiscount.setText(String.valueOf(discount));
            total = subTotal - (discount*subTotal)/100 ;
            lSubTotal.setText(String.valueOf(subTotal));
            lTotal.setText(String.valueOf(total));
            long totalPriceAfterDiscount = Long.parseLong(selected.getPrice())*Long.parseLong(jQuantity.getText())- (Long.parseLong(selected.getPrice())*Long.parseLong(jQuantity.getText())*discount)/100;
            ordering = new Order(totalPriceAfterDiscount,Controller.employee.getId(),cusId,selected.getTic_Id(),Integer.parseInt(jQuantity.getText()),LocalDateTime.now());
            listOrders.add(ordering);
        }
    }
    public void editOrder(ActionEvent event) {
        deleteToCart(event);
        addToCart(event);
    }

    public void deleteToCart(ActionEvent event) {
        subTotal-=Long.parseLong(tableItem.getSelectionModel().getSelectedItem().getPrice())*Long.parseLong(tableItem.getSelectionModel().getSelectedItem().getQuantity());
        total = subTotal - (discount*subTotal)/100;
        lSubTotal.setText(String.valueOf(subTotal));
        lTotal.setText(String.valueOf(total));
        itemList.remove(tableItem.getSelectionModel().getSelectedItem());
        for(int i=0;i<listOrders.size();i++) {
            if(tableItem.getSelectionModel().getSelectedItem().getTic_Id().equals(listOrders.get(i).getTic_Id())) {
                listOrders.remove(listOrders.get(i));
                break;
            }
        }
    }
    @FXML
    private void chargeOrder(ActionEvent event) {
        Alert alert;
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setContentText("Do you want to charge this orders. This record will be undone!!!");
        ButtonType buttonTypeOK = new ButtonType("OK",ButtonBar.ButtonData.OK_DONE);
        ButtonType buttonTypeCancel = new ButtonType("Cancel",ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(buttonTypeCancel,buttonTypeOK);
        Optional<ButtonType> response = alert.showAndWait();
        if(response.get()== buttonTypeOK) {
            boolean bool = PointOfSalesModel.createNewOrder(listOrders);
            if(bool) {
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText(null);
                alert.setContentText("Complete Order");
                alert.showAndWait();
                Controller.changeViewPOS(event);
            }
            else {
                alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setContentText("Complete Order Fail");
                alert.showAndWait();
            }
        }
    }
    @FXML
    private void setNameUser() {
        lNameUser.setText(Controller.employee.getName());
    }
}

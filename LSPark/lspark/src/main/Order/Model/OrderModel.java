package main.Order.Model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.JDBCConection;
import main.Order.Order;
import main.Order.Pos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class OrderModel {
    public static ObservableList<Order> fetchAllOrders() {
        Connection connection = JDBCConection.getJDBCConection();
        ObservableList<Order> listOrders = FXCollections.observableArrayList();
        String sqlFetchAll = "SELECT * FROM BILL";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlFetchAll);
            ResultSet res = preparedStatement.executeQuery();
            while(res.next()) {
                String orderId = res.getString("B_ID");
                String emp_Id = res.getString("B_EMP_ID");
                String cus_Id = res.getString("B_CUS_ID");
                String tic_Id = res.getString("B_TIC_ID");
                int quantity = res.getInt("QUANTITY");
                long totalPrice = res.getLong("TOTAL_PRICE");
                LocalDateTime timeOfTrans = res.getTimestamp("DATE_UPDATE").toLocalDateTime();
                listOrders.add(new Order(orderId,totalPrice,emp_Id,cus_Id,tic_Id,quantity,timeOfTrans));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listOrders;
    }
}

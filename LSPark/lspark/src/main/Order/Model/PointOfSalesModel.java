package main.Order.Model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.JDBCConection;
import main.Order.Order;
import main.Order.Pos;

import java.sql.*;

public class PointOfSalesModel {
    public static ObservableList<Pos> fetchAllItem() {
        Connection connection = JDBCConection.getJDBCConection();
        ObservableList<Pos> listPos = FXCollections.observableArrayList();
        String sqlFetchAll = "SELECT TIC_ID, TIC_NAME, PRICE, QUANTITY FROM TICKET T, WAREHOUSE W WHERE T.TIC_ID = W.WH_TIC_ID";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlFetchAll);
            ResultSet res = preparedStatement.executeQuery();
            while (res.next()) {
                String emp_id = res.getString("TIC_ID");
                String name = res.getString("TIC_NAME");
                String price = res.getString("PRICE");
                String quantity = res.getString("QUANTITY");
                listPos.add(new Pos(emp_id, name, quantity, price));
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return listPos;
    }
    public static boolean createNewOrder(ObservableList<Order> listOrder) {
        long sequence = 0;
        int count =0;
        Connection connection = JDBCConection.getJDBCConection();
        String sqlSequence = "SELECT BILL_ID.NEXTVAL FROM DUAL";
        String sqlCreate = "INSERT INTO BILL (B_ID, B_TIC_ID, QUANTITY, TOTAL_PRICE, DATE_UPDATE, B_EMP_ID, B_CUS_ID) VALUES (?,?,?,?,?,?,?)";
        try {
          PreparedStatement preparedStatement1 = connection.prepareStatement(sqlSequence);
          ResultSet res = preparedStatement1.executeQuery();
          while(res.next()) {
              sequence = res.getLong("NEXTVAL");
          }
          try {
              for(int i=0;i<listOrder.size();i++) {
                  PreparedStatement preparedStatement2 = connection.prepareStatement(sqlCreate);
                  preparedStatement2.setString(1, String.valueOf(sequence));
                  preparedStatement2.setString(2,listOrder.get(i).getTic_Id());
                  preparedStatement2.setInt(3,listOrder.get(i).getQuantity());
                  preparedStatement2.setLong(4,listOrder.get(i).getTotalPrice());
                  preparedStatement2.setTimestamp(5, Timestamp.valueOf(listOrder.get(i).getTimeOfTrans()));
                  preparedStatement2.setString(6,listOrder.get(i).getEmp_Id());
                  preparedStatement2.setString(7,listOrder.get(i).getCus_Id());
                  int bool = preparedStatement2.executeUpdate();
                  if(bool==1)
                      count++;
              }
              if(count == listOrder.size())
                  return true;
          } catch (SQLException exception) {
              exception.printStackTrace();
          }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return false;
    }
}

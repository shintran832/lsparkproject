package main.Order;

import java.time.LocalDateTime;
import java.time.LocalTime;

public class Order {
    String order_Id, emp_Id, cus_Id, tic_Id;
    int quantity;
    long totalPrice;
    LocalDateTime timeOfTrans;

    public Order(String order_Id, long totalPrice, String emp_Id, String cus_Id, String tic_Id, int quantity, LocalDateTime timeOfTrans) {
        this.order_Id = order_Id;
        this.totalPrice = totalPrice;
        this.emp_Id = emp_Id;
        this.cus_Id = cus_Id;
        this.tic_Id = tic_Id;
        this.quantity = quantity;
        this.timeOfTrans = timeOfTrans;
    }

    public Order(long totalPrice, String emp_Id, String cus_Id, String tic_Id, int quantity, LocalDateTime timeOfTrans) {
        this.totalPrice = totalPrice;
        this.emp_Id = emp_Id;
        this.cus_Id = cus_Id;
        this.tic_Id = tic_Id;
        this.quantity = quantity;
        this.timeOfTrans = timeOfTrans;
    }

    public Order(String order_Id, long totalPrice, String emp_Id, String cus_Id, String tic_Id, LocalDateTime timeOfTrans) {
        this.order_Id = order_Id;
        this.totalPrice = totalPrice;
        this.emp_Id = emp_Id;
        this.cus_Id = cus_Id;
        this.tic_Id = tic_Id;
        this.timeOfTrans = timeOfTrans;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getOrder_Id() {
        return order_Id;
    }

    public void setOrder_Id(String order_Id) {
        this.order_Id = order_Id;
    }

    public long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getEmp_Id() {
        return emp_Id;
    }

    public void setEmp_Id(String emp_Id) {
        this.emp_Id = emp_Id;
    }

    public String getCus_Id() {
        return cus_Id;
    }

    public void setCus_Id(String cus_Id) {
        this.cus_Id = cus_Id;
    }

    public String getTic_Id() {
        return tic_Id;
    }

    public void setTic_Id(String tic_Id) {
        this.tic_Id = tic_Id;
    }

    public LocalDateTime getTimeOfTrans() {
        return timeOfTrans;
    }

    public void setTimeOfTrans(LocalDateTime timeOfTrans) {
        this.timeOfTrans = timeOfTrans;
    }
}

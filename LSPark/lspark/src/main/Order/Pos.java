package main.Order;

public class Pos {
    private String tic_Id, name, quantity, price;

    public Pos(String tic_Id, String name, String quantity, String price) {
        this.tic_Id = tic_Id;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public String getTic_Id() {
        return tic_Id;
    }

    public void setTic_Id(String tic_Id) {
        this.tic_Id = tic_Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}

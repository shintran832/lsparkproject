package main.Ticket.Controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import main.Controller;
import main.Employee.Controller.ModalDetailController;
import main.Employee.Employee;
import main.Employee.Model.EmployeeModel;
import main.Ticket.Ticket;
import main.Ticket.Model.TicketModel;
import main.Ticket.Model.TicketCreateModel;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class TicketController implements Initializable {
//    Khai báo biến cho tableView và 4 table Column
    @FXML
    private TableView<Ticket> table;
    @FXML
    private TableColumn<Ticket,String> idColumn;
    @FXML
    private TableColumn<Ticket,String> nameColumn;
    @FXML
    private TableColumn<Ticket,String> typeColumn;
    @FXML
    private TableColumn<Ticket,String> priceColumn;

    @FXML
    private TextField tfSearchTicket;
    private ObservableList<Ticket> list = FXCollections.observableArrayList(TicketModel.fetchAllTicket());
    int totalFoodTic =0;
    int totalGameTic =0;
    int totalEntranceTic =0;
    int totalPackageTic =0;
    @FXML
    private Label lTotalFoodTic;
    @FXML
    private Label lTotalGameTic;
    @FXML
    private Label lTotalEntranceTic;
    @FXML
    private Label lTotalPackageTic;
    @FXML
    private Label lNameUser;


// Constructor thực hiện việc thêm danh sách ticket vào tableView , sort theo idColumn, tra cứu
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initData();
        initInfoCalculation();
        table.getSortOrder().addAll(idColumn);
        advanceSearch();
        setNameUser();
    }
//    Viet cac ham changeView o day
@FXML
public void changeViewDashboard(ActionEvent event) {
    Controller.changeViewDashboard(event);
}
    @FXML
    public void changeViewCustomer(ActionEvent event) {
        Controller.changeViewCustomer(event);
    }
    @FXML
    public void changeViewEmployee(ActionEvent event) {
        Controller.changeViewEmployee(event);
    }
    @FXML
    public void changeViewBusiness(ActionEvent event) {
        Controller.changeViewBusiness(event);
    }
    @FXML
    public void changeViewOrder(ActionEvent event) {
        Controller.changeViewOrder(event);
    }
    @FXML
    public void changeViewLogin(ActionEvent event) {
        Controller.changeViewLogin(event);
    }
    @FXML
    public void changeViewPOS(ActionEvent event) {
        Controller.changeViewPOS(event);
    }
    @FXML
    public void changeViewWarehouse(ActionEvent event) {
        Controller.changeViewWarehouse(event);
    }
//    Thong ke director,manager,employee


//    Hàm thêm danh sách nhân viên vào tableView
    @FXML
    private void initData() {
        this.idColumn.setCellValueFactory(new PropertyValueFactory<Ticket, String>("id"));
        this.nameColumn.setCellValueFactory(new PropertyValueFactory<Ticket, String>("name"));
        this.typeColumn.setCellValueFactory(new PropertyValueFactory<Ticket, String>("type"));
        this.priceColumn.setCellValueFactory(new PropertyValueFactory<Ticket, String>("price"));
        table.setItems(list);
    }
    @FXML
    public void initInfoCalculation() {
        for (int i=0;i<list.size();i++) {
            if(list.get(i).getType().equals("Food"))
                totalFoodTic += 1;
            else if(list.get(i).getType().equals("Game"))
                totalGameTic += 1;
            else if(list.get(i).getType().equals("Entrance"))
                totalEntranceTic += 1;
            else
                totalPackageTic += 1;
        }
        lTotalFoodTic.setText(String.valueOf(totalFoodTic));
        lTotalGameTic.setText(String.valueOf(totalGameTic));
        lTotalEntranceTic.setText(String.valueOf(totalEntranceTic));
        lTotalPackageTic.setText(String.valueOf(totalPackageTic));
    }
    // Chuyển sang trang Thêm ve'
    @FXML
    private void changeViewTicketCreate(ActionEvent event) {
        try {
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../View/TicketCreate.fxml"));
            Parent modalCreate = loader.load();
            Scene scene = new Scene(modalCreate);
            stage.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
// Xoá ve'
    @FXML
    private void delTicket(ActionEvent event) {
        Ticket selected = table.getSelectionModel().getSelectedItem();
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setHeaderText(null);
        alert.setContentText("Do you really want to delete these record? This process cannot be undone");
        ButtonType buttonTypeOK = new ButtonType("OK",ButtonBar.ButtonData.OK_DONE);
        ButtonType buttonTypeCancel = new ButtonType("Cancel",ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(buttonTypeCancel,buttonTypeOK);
        Optional<ButtonType> response = alert.showAndWait();
        if(response.get()== buttonTypeOK) {
            boolean res = TicketModel.delTicket(selected);
            if (res) {
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText(null);
                alert.setContentText("Delete Ticket Success!!");
                alert.showAndWait();
                list.remove(selected);
                if(selected.getType().equals("Food")) {
                    totalFoodTic-=1;
                    lTotalFoodTic.setText(String.valueOf(totalFoodTic));
                }
                else if(selected.getType().equals("Entrance")) {
                    totalEntranceTic-=1;
                    lTotalEntranceTic.setText(String.valueOf(totalEntranceTic));
                }
                else if(selected.getType().equals("Package")) {
                    totalPackageTic-=1;
                    lTotalPackageTic.setText(String.valueOf(totalPackageTic));
                }
                else {
                    totalGameTic-=1;
                    lTotalGameTic.setText(String.valueOf(totalGameTic));
                }
            } else {
                alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setContentText("Error! Delete Ticket fail!");
                alert.showAndWait();
            }
        }
    }
//    Chuyen sang trang Cập nhật ve'
    @FXML
    private void changeViewTicketDetail(ActionEvent event) {
        try {
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../View/TicketDetail.fxml"));
            Parent modalCreate = loader.load();
            Scene scene = new Scene(modalCreate);
            TicketDetailController controller = loader.getController();
            Ticket selected = table.getSelectionModel().getSelectedItem();
            controller.setTicket(selected);
            stage.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
//    Tra cứu
    @FXML
    private void advanceSearch() {
        FilteredList<Ticket> filteredList = new FilteredList<>(list, b -> true);
        tfSearchTicket.textProperty().addListener((observable , oldValue, newValue) -> {
            filteredList.setPredicate(ticket -> {
                if(newValue ==null || newValue.isEmpty()) {
                    return true;
                }
                String lowCaseFilter = newValue.toLowerCase();
                if(ticket.getId().toLowerCase().indexOf(lowCaseFilter) != -1 ) {
                    return true;
                }
                else if(ticket.getName().toLowerCase().indexOf(lowCaseFilter)!= -1) {
                    return true;
                }
                else if (ticket.getType().toLowerCase().indexOf(lowCaseFilter) != -1) {
                    return true;
                }
                else if (String.valueOf(ticket.getPrice()).toLowerCase().indexOf(lowCaseFilter) != -1) {
                    return true;
                }
                else if(ticket.getDescription().toLowerCase().indexOf(lowCaseFilter) != -1) {
                    return true;
                }
                else
                    return false;
            });
        });
        SortedList<Ticket> sortData = new SortedList<>(filteredList);
        sortData.comparatorProperty().bind(table.comparatorProperty());
        table.setItems(sortData);
        table.getSortOrder().add(idColumn);
    }
    @FXML
    private void setNameUser() {
        lNameUser.setText(Controller.employee.getName());
    }

}

package main.Ticket.Controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import main.Controller;
import main.Ticket.Ticket;
import main.Ticket.Model.TicketCreateModel;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.ResourceBundle;

public class TicketCreateController implements Initializable {
    @FXML
    private TextField id;
    @FXML
    private TextField name;
    @FXML
    private ComboBox<String> comboBox ;
    ObservableList<String> typeList = FXCollections.observableArrayList("Game","Entrance","Food","Package");
    @FXML
    private TextField price;
    @FXML
    private TextArea description;

// Constructor thực hiện setItem( gồm male và female cho genderList )
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        comboBox.setItems(typeList);
    }

    @FXML
    private void createNewTicket(ActionEvent event) {
        String idTic = id.getText();
        String nameTic = name.getText();
        String type = comboBox.getValue();
        long priceTic = Long.parseLong(price.getText());
        String descrriptionTic = description.getText();
        Alert alert;
        if(idTic.isEmpty() || nameTic.isEmpty() || type.isEmpty() || String.valueOf(priceTic).isEmpty()) {
            alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setContentText("You are missing some data!");
            alert.showAndWait();
            return;
        }
        Ticket ticNew = new Ticket(idTic,nameTic,type,priceTic,descrriptionTic);
        boolean res;
        if(ticNew.getType().equals("Food")) {
            res = TicketCreateModel.createNewTicket(ticNew);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM dd yyyy");
            LocalDate today = LocalDate.now();
            String toString = today.format(formatter);
            LocalDate parseToday = LocalDate.parse(toString,formatter);
            TicketCreateModel.createWareHouse(ticNew.getId(),Controller.employee.getId(),0,parseToday);
        }
        else {
            res = TicketCreateModel.createNewTicket(ticNew);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM dd yyyy");
            LocalDate today = LocalDate.now();
            String toString = today.format(formatter);
            LocalDate parseToday = LocalDate.parse(toString,formatter);
            TicketCreateModel.createWareHouse(ticNew.getId(),Controller.employee.getId(),1000,parseToday);
        }
        if(res == true) {
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Create New Ticket Success!!!");
            alert.showAndWait();
            changeViewTicket(event);
        }
        else {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("Error Invalid information !!!");
            alert.showAndWait();
        }
    }
    @FXML
    private void changeViewTicket(ActionEvent event) {
        Controller.changeViewTicket(event);
    }


}

package main.Ticket.Controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import main.Controller;

import main.Customer.Customer;
import main.Ticket.Model.TicketCreateModel;

import main.Ticket.Model.TicketDetailModel;
import main.Ticket.Ticket;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
public class TicketDetailController implements Initializable {
    @FXML
    private TextField name;
    @FXML
    private TextField price;
    @FXML
    private TextArea description;

    private String ID = "";

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
    public void setTicket(Ticket selected) {
        name.setText(selected.getName());
        price.setText(String.valueOf(selected.getPrice()));
        description.setText(selected.getDescription());
        ID = selected.getId();
    }

    public void updateTicket(ActionEvent event) {
        String nameTic = name.getText();
        long priceTic = Long.parseLong(price.getText());
        String descriptionTic = description.getText();
        Alert alert;
        if(nameTic.isEmpty() || String.valueOf(priceTic).isEmpty()) {
            alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setContentText("You are missing some data!");
            alert.showAndWait();
            return;
        }
       Ticket ticNew = new Ticket(nameTic,priceTic,descriptionTic);
        boolean res = TicketDetailModel.updateTicket(ticNew, ID);
        if(res == true) {
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Update New TicKet Success");
            alert.showAndWait();
            changeViewTicket(event);
        }
        else {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("Error!! Invalid information");
            alert.showAndWait();
        }
    }
    @FXML
    private void changeViewTicket(ActionEvent event) {
        Controller.changeViewTicket(event);
    }
}

package main.Ticket.Model;

import main.Ticket.Ticket;
import main.JDBCConection;
import oracle.jdbc.proxy.annotation.Pre;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;

public class TicketCreateModel {
    public static boolean createNewTicket(Ticket ticNew) {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlCreate = "INSERT INTO TICKET (TIC_ID, TIC_NAME, TYPE, PRICE, DESCRIPTION) VALUES (?,?,?,?,?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlCreate);
            preparedStatement.setString(1,ticNew.getId());
            preparedStatement.setString(2,ticNew.getName());
            preparedStatement.setString(3,ticNew.getType());
            preparedStatement.setLong(4,ticNew.getPrice());
            preparedStatement.setString(5,ticNew.getDescription());
            int res = preparedStatement.executeUpdate();
            if(res ==1) return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public static void createWareHouse(String ticId, String empId, int quantity, LocalDate timeOfTransaction) {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlCreateWarehouse = "INSERT INTO WAREHOUSE(WH_ID,WH_EMP_ID,WH_TIC_ID,DATE_UPDATE,QUANTITY,DESCRIPTION) VALUES (WAREHOUSE_ID.NEXTVAL,?,?,?,?,NULL)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlCreateWarehouse);
            preparedStatement.setString(1,empId);
            preparedStatement.setString(2,ticId);
            preparedStatement.setDate(3, Date.valueOf(timeOfTransaction));
            preparedStatement.setInt(4,quantity);
            int res = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}

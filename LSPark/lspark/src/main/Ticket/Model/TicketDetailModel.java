package main.Ticket.Model;

import javafx.fxml.FXML;
import main.Employee.Employee;
import main.JDBCConection;
import main.Ticket.Ticket;

import java.sql.*;
import java.time.LocalDate;

public class TicketDetailModel {

    @FXML
    public static boolean updateTicket(Ticket tic, String ticID) {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlUpdate = "UPDATE TICKET SET TIC_NAME=?, PRICE=?, DESCRIPTION=? WHERE TIC_ID=?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlUpdate);
            preparedStatement.setString(1,tic.getName());
            preparedStatement.setLong(2,tic.getPrice());
            preparedStatement.setString(3,tic.getDescription());
            preparedStatement.setString(4,ticID);
            int res = preparedStatement.executeUpdate();
            if(res ==1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
//    public static void updateWarehouse(String oldTicId, String newTicId, String oldEmpId, String newEmpId, LocalDate timeOfTransaction) {
//        String quantity;
//
//        Connection connection = JDBCConection.getJDBCConection();
//        String sqlUpdate = "UPDATE WAREHOUSE SET WH_EMP_ID=? AND WH_TIC_ID=? DATE_UPDATE=? WHERE WH_EMP_ID=? AND WH_TIC_ID=?";
//        try {
//            PreparedStatement preparedStatement = connection.prepareStatement(sqlUpdate);
//            preparedStatement.setString(1,newEmpId);
//            preparedStatement.setString(2,newTicId);
//            preparedStatement.setDate(3, Date.valueOf(timeOfTransaction));
//            preparedStatement.setString(4,oldEmpId);
//            preparedStatement.setString(5,oldTicId);
//            int res = preparedStatement.executeUpdate();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
}

package main.Ticket.Model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import jdk.nashorn.internal.scripts.JD;
import main.Ticket.Ticket;
import main.JDBCConection;
//import sun.security.krb5.internal.Ticket;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TicketModel {
//    LOAD DATA
    public static ObservableList<Ticket> fetchAllTicket() {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlFetchAll = "SELECT * FROM TICKET";
        ObservableList<Ticket> listTickets = FXCollections.observableArrayList();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlFetchAll);
            ResultSet res = preparedStatement.executeQuery();
            while (res.next()) {
                String id = res.getString("TIC_ID");
                String name = res.getString("TIC_NAME");
                String type = res.getString("TYPE");
                long price = res.getLong("PRICE");
                String description = res.getString("DESCRIPTION");
                listTickets.add(new Ticket(id,name,type,price,description));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listTickets;
    }
    public static boolean delTicket(Ticket tic) {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlDelete = "DELETE FROM TICKET WHERE TIC_ID=?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlDelete);
            preparedStatement.setString(1,tic.getId());
            int res = preparedStatement.executeUpdate();
            if(res==1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public static Ticket getInfo(String ID) {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlGetInfo = "SELECT * FROM TICKET WHERE TIC_ID = ?";
        Ticket ticket = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlGetInfo);
            preparedStatement.setString(1, ID);
            ResultSet res = preparedStatement.executeQuery();
            while (res.next()) {
                String id = res.getString("TIC_ID");
                String name = res.getString("TIC_NAME");
                String type = res.getString("TYPE");
                long price = res.getLong("PRICE");
                String description = res.getString("DESCRIPTION");
                ticket = new Ticket(id,name,type,price,description);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ticket;
    }

}

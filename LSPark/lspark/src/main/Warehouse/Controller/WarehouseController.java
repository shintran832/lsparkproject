package main.Warehouse.Controller;

import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import main.Controller;
import main.Employee.Employee;
import main.Employee.Model.EmployeeModel;
import main.Warehouse.Model.WarehouseModel;
import main.Warehouse.Warehouse;

import javax.sound.midi.ControllerEventListener;
import javax.swing.*;
import java.awt.*;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

public class WarehouseController implements Initializable {
    @FXML
    private TableView<Warehouse> table;
    @FXML
    private TableColumn<Warehouse,String> empIdColumn;
    @FXML
    private TableColumn<Warehouse,String> ticIdColumn;
    @FXML
    private TableColumn<Warehouse, Date> dateUpdateColumn;
    @FXML
    private TableColumn<Warehouse,String> quantityColumn;
    @FXML
    private Label lNameUser;
    public ObservableList<Warehouse> list = FXCollections.observableArrayList(WarehouseModel.fetchAllWarehouses());

    @FXML
    JFXTextField quantity;
    @FXML
    JFXTextField jTicId;
    @FXML
    JFXTextField description;
    @FXML
    TextField tfSearch;
    Warehouse war ;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initData();
        advanceSearch();
        SelectItem();
        setNameUser();
    }
    public void initData() {
        this.empIdColumn.setCellValueFactory(new PropertyValueFactory<Warehouse, String>("emp_Id"));
        this.ticIdColumn.setCellValueFactory(new PropertyValueFactory<Warehouse, String>("tic_Id"));
        this.quantityColumn.setCellValueFactory(new PropertyValueFactory<Warehouse, String>("quantity"));
        this.dateUpdateColumn.setCellValueFactory(new PropertyValueFactory<Warehouse, Date>("date_Update"));
        table.setItems(list);
    }
    @FXML
    private void updateWarehouse(ActionEvent event) {
        int quantityTic = Integer.parseInt(quantity.getText());
        String descriptionTic = description.getText();
        Alert alert;
        if(String.valueOf(quantityTic).isEmpty()) {
            alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setContentText("You are missing some data!");
            alert.showAndWait();
            return;
        }
        else {
            boolean res = WarehouseModel.updateWarehouse(Controller.employee.getId(),quantityTic,war.getWar_Id(),descriptionTic);
            if(res) {
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText(null);
                alert.setContentText("Update New Warehouse Success");
                alert.showAndWait();
                Controller.changeViewWarehouse(event);
            }
            else {
                alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setContentText("Error!! Invalid information");
                alert.showAndWait();
            }
        }
    }
    @FXML
    private void advanceSearch() {
        FilteredList<Warehouse> filteredList = new FilteredList<>(list, b -> true);
        tfSearch.textProperty().addListener((observable , oldValue, newValue) -> {
            filteredList.setPredicate(warehouse -> {
                if(newValue ==null || newValue.isEmpty()) {
                    return true;
                }
                String lowCaseFilter = newValue.toLowerCase();
                if(warehouse.getEmp_Id().toLowerCase().indexOf(lowCaseFilter) != -1 ) {
                    return true;
                }
                else if(warehouse.getTic_Id().toLowerCase().indexOf(lowCaseFilter)!= -1) {
                    return true;
                }
                else if (String.valueOf(warehouse.getQuantity()).toLowerCase().indexOf(lowCaseFilter) != -1) {
                    return true;
                }
                else if (String.valueOf(warehouse.getDate_Update()).toLowerCase().indexOf(lowCaseFilter) != -1) {
                    return true;
                }
                else
                    return false;
            });
        });
        SortedList<Warehouse> sortData = new SortedList<>(filteredList);
        sortData.comparatorProperty().bind(table.comparatorProperty());
        table.setItems(sortData);
        table.getSortOrder().add(ticIdColumn);
    }
    @FXML
    private void SelectItem() {
        table.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                war = table.getSelectionModel().getSelectedItem();
                jTicId.setText(war.getTic_Id());
                quantity.setText(String.valueOf(war.getQuantity()));
                description.setText(war.getDescription());
            }
        });
    }
    @FXML
    public void changeViewDashboard(ActionEvent event) {
        Controller.changeViewDashboard(event);
    }
    @FXML
    public void changeViewCustomer(ActionEvent event) {
        Controller.changeViewCustomer(event);
    }
    @FXML
    public void changeViewEmployee(ActionEvent event) {
        Controller.changeViewEmployee(event);
    }
    @FXML
    public void changeViewBusiness(ActionEvent event) {
        Controller.changeViewBusiness(event);
    }
    @FXML
    public void changeViewTicket(ActionEvent event) {
        Controller.changeViewTicket(event);
    }
    @FXML
    public void changeViewLogin(ActionEvent event) {
        Controller.changeViewLogin(event);
    }
    @FXML
    public void changeViewPOS(ActionEvent event) {
        Controller.changeViewPOS(event);
    }
    @FXML
    public void changeViewOrder(ActionEvent event) {
        Controller.changeViewOrder(event);
    }
    @FXML
    public void refreshData(ActionEvent event) {
        Controller.changeViewWarehouse(event);
    }
    @FXML
    private void setNameUser() {
        lNameUser.setText(Controller.employee.getName());
    }
}


package main.Warehouse.Model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.JDBCConection;
import main.Warehouse.Warehouse;

import java.sql.*;
import java.time.LocalDate;

public class WarehouseModel {
    public static ObservableList<Warehouse> fetchAllWarehouses() {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlFetchAll = "SELECT * FROM WAREHOUSE";
        ObservableList<Warehouse> listWarehouses = FXCollections.observableArrayList();
        ObservableList<Warehouse> list = FXCollections.observableArrayList();
        try {
//            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlFetchAll);
            ResultSet res = preparedStatement.executeQuery();
            while (res.next()) {
                String war_Id = res.getString("WH_ID");
                String emp_Id = res.getString("WH_EMP_ID");
                String tic_Id = res.getString("WH_TIC_ID");
                LocalDate date_Update = res.getDate("DATE_UPDATE").toLocalDate();
                int quantity = res.getInt("QUANTITY");
                String description = res.getString("DESCRIPTION");
                listWarehouses.add(new Warehouse(war_Id,emp_Id,tic_Id,quantity,description,date_Update));
            }
            for(int i=0;i<listWarehouses.size();i++) {
                if(!(listWarehouses.get(i).getQuantity()<0)) {
                    list.add(listWarehouses.get(i));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException exception) {
                exception.printStackTrace();
            }

        }
        return list;
    }
//    public static boolean updateWarehouse(String emp_id,int quantity, String war_Id, String description) {
//        Connection connection = JDBCConection.getJDBCConection();
//        String sqlUpdate = "UPDATE WAREHOUSE SET QUANTITY=?, DESCRIPTION=?, WH_EMP_ID=? WHERE WH_ID=?";
//        try {
////            connection.setAutoCommit(false);
//            PreparedStatement preparedStatement = connection.prepareStatement(sqlUpdate);
//            preparedStatement.setInt(1,quantity);
//            preparedStatement.setString(2,description);
//            preparedStatement.setString(3,emp_id);
//            preparedStatement.setString(4,war_Id);
//            int res = preparedStatement.executeUpdate();
////            connection.commit();
//            if(res==1) return true;
//        } catch (SQLException e) {
//            e.printStackTrace();
//            try {
//                connection.rollback();
//            } catch (SQLException exception) {
//                exception.printStackTrace();
//            }
//        }
//        return false;
//    }
    public static boolean updateWarehouse(String emp_id,int quantity, String war_Id, String description) {
        Connection connection = JDBCConection.getJDBCConection();
        String sqlUpdateCall = "{call UPDATE_QUANTITY_WAREHOUSE(?,?,?,?)}";
        try {
            CallableStatement callUpdate = connection.prepareCall(sqlUpdateCall);
            callUpdate.setString(1,war_Id);
            callUpdate.setInt(2,quantity);
            callUpdate.setString(3,emp_id);
            callUpdate.setString(4,description);
            int res = callUpdate.executeUpdate();
            if(res==1) return true;
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return false;
    }

}

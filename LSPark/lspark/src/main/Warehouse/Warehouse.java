package main.Warehouse;

import java.time.LocalDate;

public class Warehouse {
    private String war_Id, emp_Id, tic_Id,description;
    private LocalDate date_Update;
    private int quantity;

    public Warehouse(String war_Id, String emp_Id, String tic_Id, int quantity, String description, LocalDate date_Update) {
        this.war_Id = war_Id;
        this.emp_Id = emp_Id;
        this.tic_Id = tic_Id;
        this.quantity = quantity;
        this.description = description;
        this.date_Update = date_Update;
    }

    public Warehouse(String emp_Id, String tic_Id, int quantity, String description, LocalDate date_Update) {
        this.emp_Id = emp_Id;
        this.tic_Id = tic_Id;
        this.quantity = quantity;
        this.description = description;
        this.date_Update = date_Update;
    }

    public Warehouse(String emp_Id, String tic_Id, int quantity, LocalDate date_Update) {
        this.emp_Id = emp_Id;
        this.tic_Id = tic_Id;
        this.quantity = quantity;
        this.date_Update = date_Update;
    }

    public String getWar_Id() {
        return war_Id;
    }

    public void setWar_Id(String war_Id) {
        this.war_Id = war_Id;
    }

    public String getEmp_Id() {
        return emp_Id;
    }

    public void setEmp_Id(String emp_Id) {
        this.emp_Id = emp_Id;
    }

    public String getTic_Id() {
        return tic_Id;
    }

    public void setTic_Id(String tic_Id) {
        this.tic_Id = tic_Id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDate_Update() {
        return date_Update;
    }

    public void setDate_Update(LocalDate date_Update) {
        this.date_Update = date_Update;
    }
}
